/** fizz buzz
 *
 * A program that prints the numbers from 1 to 100. But for multiples
 * of three prints “Fizz” instead of the number and for the multiples
 * of five prints “Buzz”. For numbers which are multiples of both three
 * and five prints “FizzBuzz”.
 *
 */

fn main() {
    let mut i: i32 = 1;
    while i <= 100 {
        let fizz =  div_3(i);
        let buzz =  div_5(i);
        
        if fizz && buzz {
            println!("FizzBuzz");
        } else if fizz {
            println!("Fizz");
        } else if buzz {
            println!("Buzz");
        } else {
            println!("{}", i);
        }
        i = i + 1;
    }
}

fn div_3(num: i32) -> bool {
    if num % 3 == 0 {
        true
    } else {
        false
    }
}

fn div_5(num: i32) -> bool {
    if num % 5 == 0 {
        true
    } else {
        false
    }
}
